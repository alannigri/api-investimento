package br.com.investimento.api.services;


import br.com.investimento.api.DTO.RespostaDTO;
import br.com.investimento.api.models.Investimento;
import br.com.investimento.api.models.Simulacao;
import br.com.investimento.api.repositories.InvestimentoRepository;
import br.com.investimento.api.repositories.SimulacaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.CriteriaBuilder;
import java.util.Optional;

@Service
public class SimulacaoService {

    @Autowired
    private SimulacaoRepository simulacaoRepository;

    @Autowired
    private InvestimentoService investimentoService;

    @Autowired
    private InvestimentoRepository investimentoRepository;

    public RespostaDTO criarSimulacao(int codigoInvestimento, Simulacao simulacao) {
           Investimento investimento = investimentoService.buscarPorID(codigoInvestimento);
        double montante = 0;
        for (int i = 0; i < simulacao.getQuantidadeMeses(); i++) {
            if (i == 0) {
                montante = simulacao.getValorAplicado();
            }
            montante += montante * (investimento.getRendimentoPorMes());
        }
        RespostaDTO respostaDTO = new RespostaDTO();
        respostaDTO.setRendimentoPorMes(investimento.getRendimentoPorMes());
        respostaDTO.setMontante(montante);
        simulacao.setInvestimentos(investimento);
        simulacaoRepository.save(simulacao);
        return respostaDTO;
    }

    public Iterable<Simulacao> buscarTodasSimulacoes(){
        Iterable<Simulacao> simulacoes = simulacaoRepository.findAll();
        return simulacoes;
    }
}
