package br.com.investimento.api.services;

import br.com.investimento.api.models.Investimento;
import br.com.investimento.api.repositories.InvestimentoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class InvestimentoService {

    @Autowired
    private InvestimentoRepository investimentoRepository;

    public Investimento criarInvestimento(Investimento investimento) {
        Investimento investimentoObjeto = investimentoRepository.save(investimento);
        return investimentoObjeto;
    }

    public Iterable<Investimento> buscaTodosInvestimentos() {
        Iterable<Investimento> investimentos = investimentoRepository.findAll();
        return investimentos;
    }

    public Investimento buscarPorID (int codigoInvestimento) throws RuntimeException{
        Optional<Investimento> optionalInvestimento = investimentoRepository.findById(codigoInvestimento);
        if(!optionalInvestimento.isPresent()) {
            throw new RuntimeException("Não foi encontrado o código de investimento");
        }
        Investimento investimento = optionalInvestimento.get();
        return investimento;
    }

}
