package br.com.investimento.api.controllers;

import br.com.investimento.api.DTO.RespostaDTO;
import br.com.investimento.api.models.Investimento;
import br.com.investimento.api.models.Simulacao;
import br.com.investimento.api.services.InvestimentoService;
import br.com.investimento.api.services.SimulacaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping("/investimentos")
public class InvestimentoController {

    @Autowired
    private InvestimentoService investimentoService;

    @Autowired
    private SimulacaoService simulacaoService;

    @GetMapping
    public Iterable<Investimento> buscaTodosInvestimentos(){
        Iterable<Investimento> investimentos = investimentoService.buscaTodosInvestimentos();
        return investimentos;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Investimento criaInvestimento (@RequestBody @Valid Investimento investimento){
            Investimento investimentoObjeto = investimentoService.criarInvestimento(investimento);
            return investimentoObjeto;
    }

    @PostMapping("/{idInvestimento}/simulacao")
    @ResponseStatus(HttpStatus.CREATED)
    public RespostaDTO criaSimulacao (@PathVariable (name = "idInvestimento") Integer idInvestimento,
                                      @RequestBody @Valid Simulacao simulacao){
        try {
            RespostaDTO respostaDTO = simulacaoService.criarSimulacao(idInvestimento, simulacao);
            return respostaDTO;
        } catch (RuntimeException exception){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }
}
