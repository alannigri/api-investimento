package br.com.investimento.api.models;

import javax.persistence.*;
import javax.validation.constraints.*;


@Entity
@Table(name = "Simulacao")
public class Simulacao {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int codigoCliente;

    @NotNull
    @NotBlank
    private String nomeInteressado;

    @NotNull
    @Email
    private String email;

    @Digits(integer = 8, fraction = 2, message = "Valor com no máximo 2 casas decimais")
    private double valorAplicado;

    @NotNull
    @Min(value = 1, message ="Quantidade de Meses deve ser maior ou igual a 1")
    private int quantidadeMeses;


    @ManyToOne(cascade = CascadeType.ALL)
    private Investimento investimentos;


    public Simulacao() {
    }

    public int getCodigoCliente() {
        return codigoCliente;
    }

    public void setCodigoCliente(int codigoCliente) {
        this.codigoCliente = codigoCliente;
    }

    public String getNomeInteressado() {
        return nomeInteressado;
    }

    public void setNomeInteressado(String nomeInteressado) {
        this.nomeInteressado = nomeInteressado;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public double getValorAplicado() {
        return valorAplicado;
    }

    public void setValorAplicado(double valorAplicado) {
        this.valorAplicado = valorAplicado;
    }

    public int getQuantidadeMeses() {
        return quantidadeMeses;
    }

    public void setQuantidadeMeses(int quantidadeMeses) {
        this.quantidadeMeses = quantidadeMeses;
    }

    public Investimento getInvestimentos() {
        return investimentos;
    }

    public void setInvestimentos(Investimento investimentos) {
        this.investimentos = investimentos;
    }
}
