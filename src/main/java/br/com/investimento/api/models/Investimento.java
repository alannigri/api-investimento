package br.com.investimento.api.models;

import javax.persistence.*;
import javax.validation.constraints.*;


@Entity
@Table(name = "Investimentos")
public class Investimento {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int investimentoID;

    @Column(unique = true)
    @NotNull(message = "Informar o nomeInvestimento")
    @NotBlank(message = "Preencher o nomeInvestimento")
    private String nomeInvestimento;

    @NotNull(message = "Informar o rendimentoPorMes")
    @DecimalMin(value = "0.01", message = "Rendimento deve ser maior que 0.01")
    @Digits(integer = 8, fraction = 2, message = "Valor com no máximo 2 casas decimais")
    private double rendimentoPorMes;

    public Investimento() {
    }

    public int getInvestimentoID() {
        return investimentoID;
    }

    public void setInvestimentoID(int investimentoID) {
        this.investimentoID = investimentoID;
    }

    public double getRendimentoPorMes() {
        return rendimentoPorMes;
    }

    public void setRendimentoPorMes(double rendimentoPorMes) {
        this.rendimentoPorMes = rendimentoPorMes;
    }

    public String getNomeInvestimento() {
        return nomeInvestimento;
    }

    public void setNomeInvestimento(String nomeInvestimento) {
        this.nomeInvestimento = nomeInvestimento;
    }
}
