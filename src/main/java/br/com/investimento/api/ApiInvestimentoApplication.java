package br.com.investimento.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiInvestimentoApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiInvestimentoApplication.class, args);
	}

}
