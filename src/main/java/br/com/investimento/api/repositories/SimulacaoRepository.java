package br.com.investimento.api.repositories;

import br.com.investimento.api.models.Simulacao;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface SimulacaoRepository extends CrudRepository<Simulacao, Integer> {

}
