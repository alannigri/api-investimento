package br.com.investimento.api.repositories;

import br.com.investimento.api.models.Investimento;
import org.springframework.data.repository.CrudRepository;

public interface InvestimentoRepository extends CrudRepository<Investimento, Integer> {

}
