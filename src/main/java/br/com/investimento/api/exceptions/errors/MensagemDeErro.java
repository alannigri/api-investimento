package br.com.investimento.api.exceptions.errors;

import java.util.HashMap;

public class MensagemDeErro {
    private String erro;
    private String mensagemDeErro;
    private HashMap<String, ObjetoDeErro> camposDeErro;

    public MensagemDeErro(String erro, String mensagemDeErro, HashMap<String, ObjetoDeErro> camposDeErro) {
        this.erro = erro;
        this.mensagemDeErro = mensagemDeErro;
        this.camposDeErro = camposDeErro;
    }

    public MensagemDeErro() {
    }

    public String getErro() {
        return erro;
    }

    public void setErro(String erro) {
        this.erro = erro;
    }

    public String getMensagemDeErro() {
        return mensagemDeErro;
    }

    public void setMensagemDeErro(String mensagemDeErro) {
        this.mensagemDeErro = mensagemDeErro;
    }

    public HashMap<String,ObjetoDeErro> getCamposDeErro() {
        return camposDeErro;
    }

    public void setCamposDeErro(HashMap<String, ObjetoDeErro> camposDeErro) {
        this.camposDeErro = camposDeErro;
    }
}
