package br.com.investimento.api.exceptions;

import br.com.investimento.api.exceptions.errors.MensagemDeErro;
import br.com.investimento.api.exceptions.errors.ObjetoDeErro;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.HashMap;

@ControllerAdvice
public class ErrorHandler {

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(code = HttpStatus.UNPROCESSABLE_ENTITY)
    @ResponseBody
    public MensagemDeErro manipulacaoDeErrosDeValidacao(MethodArgumentNotValidException exception) {
        HashMap<String, ObjetoDeErro> erros = new HashMap<>();
        BindingResult resultado = exception.getBindingResult();

        // Varrer a lista de erros
        for (FieldError fieldError : resultado.getFieldErrors()) {
            erros.put(fieldError.getField(),
                    new ObjetoDeErro(fieldError.getDefaultMessage(), fieldError.getRejectedValue().toString()));
        }

        MensagemDeErro mensagemDeErro = new MensagemDeErro(HttpStatus.UNPROCESSABLE_ENTITY.toString(),
                "Erro de Validação dos Campos Enviados", erros);
        return mensagemDeErro;
    }
}